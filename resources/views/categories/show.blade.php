@extends('layouts.app')
@section('title', 'Detalhes da categoria')
@section('content')
<div class="container-fluid">
    <h4 class="font-weight-bold mb-5">Detalhes da categoria</h4>
    <div class="row">
        <div class="col-6">
            <h5>Categoria</h5>
            Id: {{ $category->id }}<br>
            Título: {{ $category->title }}<br>
            Descrição: {{ $category->description }}
        </div>
        <div class="col-6">
            <h5>Posts relacionados</h5>
            @if (count($posts))
                @foreach ($posts as $post)
                <hr>
                    # {{ $post->id }} : &nbsp;&nbsp;<strong>{{ $post->title }}</strong><br>
                    Sub-título: {{ $post->subtitle }}<br>
                    Conteúdo: {{ $post->content }}
                    <p class="mt-2">
                        <a class="btn btn-sm btn-danger" href="{{ route('post.destroy', ['post' => $post->id]) }}">Delete</a>
                        <a class="btn btn-sm btn-secondary" href="{{ route('post.show', ['post' => $post->id]) }}">Detalhes</a>
                    </p>
                @endforeach
            @else
                <p class="text-danger">* Não há posts para esta categoria.</p>
            @endif
        </div>
    </div>
</div>
@endsection
