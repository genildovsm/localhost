@extends('layouts.app')
@section('title','Listar usuários')
@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <h4 class="font-weight-bold text-center mb-5">Lista de categorias</h4>
            <table class="table table-striped table-sm">
                <thead class="thead-light">
                    <tr>
                        <th>ID</th>
                        <th>Título</th>
                        <th>Descrição</th>
                        <th colspan="2"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($categories as $category)
                    <tr>
                        <td>{{ $category->id }}</td>
                        <td>{{ $category->title }}</td>
                        <td>{{ $category->description }}</td>
                        <td class="text-right">
                            <a class="btn btn-secondary btn-sm" href="{{ route('category.show',['category' => $category->id]) }}">Detalhes</a>
                        </td>
                        <td class="text-right">
                            <form action="{{ route('category.destroy', ['category' => $category->id]) }}" method="post">
                                @csrf
                                @method('delete')
                                <input type="hidden" name="category" value="{{ $category->id }}">
                                <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
