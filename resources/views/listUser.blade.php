<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Usuários</title>
</head>
<body>
    <h2>Detalhes do usuário</h2>
    <p>
        Nome : {{ $user->name }}<br>
        E-mail : {{ $user->email }}<br>
        Criado em : {{ date('d/m/Y H:i', strtotime($user->created_at)) }}
    </p>
</body>
</html>