@extends('layouts.app')
@section('title', 'Novo Post')
@section('content')
<div class="container">
    <div class="row">
        <div class="offset-3 col-6">
            <h4 class="text-center ">Cadastrar Post</h4>
            <form action="{{ route('post.store') }}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="title">Título</label>
                    <input class="form-control" type="text" id="title" name="title">
                </div>
                <div class="form-group">
                    <label for="subtitle">Sub-título</label>
                    <input class="form-control" type="text" id="subtitle" name="subtitle">
                </div>
                <p>
                    <label for="content">Conteúdo</label><br>
                    <textarea class="form-control" id="content" name="content" cols="50" rows="5"></textarea>
                </p>
                <button type="submit" class="btn btn-primary btn-block">Criar novo Post</button>
            </form>
        </div>
    </div>
</div>
@endsection
