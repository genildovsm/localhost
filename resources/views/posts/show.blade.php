@extends('layouts.app')
@section('title', 'Listar Posts')
@section('content')
<div class="container-fluid">
    <h4 class="font-weight-bold">Detalhes do Post</h4>
    <div class="row mt-5">
        <div class="col-6">
            <p class="font-weight-bold">Post</p>
                ID: {{ $post->id }}<br>
                Título: {{ $post->title }}<br>
                Sub-título: {{ $post->subtitle }}<br>
                Conteúdo: {{ $post->content }}

            <p class="font-weight-bold mt-4">Autor</p>
                ID: {{ $author->id }}<br>
                Nome: {{ $author->name }}<br>
                Email: {{ $author->email }}
        </div>
        <div class="col-6">
            <p class="font-weight-bold">Categorias</p>
            @foreach ($categories as $category)
                <hr>
                    ID: {{ $category->id }}<br>
                    Título: {{ $category->title }}
                    <p class="mt-2">
                        <a class="btn btn-sm btn-secondary" href="{{ route('category.show', ['category' => $category->id]) }}">Detalhes</a>
                    </p>
            @endforeach
        </div>
    </div>
</div>
@endsection
