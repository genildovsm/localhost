@extends('layouts.app')
@section('title', 'Listar Posts')
@section('content')
<div class="container-fluid">
    <h4 class="font-weight-bold text-center mb-5">Lista de Posts</h4>
    <table class="table table-striped table-sm">
        <thead class="thead-light">
            <tr>
                <th>ID</th>
                <th>Titulo</th>
                <th>Sub-título</th>
                <th>Conteúdo</th>
                <th colspan="2"></th>
            </tr>
        </thead>
        <tbody>
            @foreach ($posts as $post)
                <tr>
                    <td>{{ $post->id }}</td>
                    <td>{{ $post->title }}</td>
                    <td>{{ $post->subtitle }}</td>
                    <td>{{ $post->content }}</td>
                    <td>
                        <a class="btn btn-secondary btn-sm" href="{{ route('post.show',['post' => $post->id]) }}">
                            Detalhes
                        </a>
                    </td>
                    <td class="text-right">
                        <form action="{{ route('post.destroy', ['post' => $post->id]) }}" method="post">
                            @csrf
                            @method('delete')
                            <input type="hidden" name="post" value="{{ $post->id }}">
                            <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection
