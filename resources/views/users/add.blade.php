@extends('layouts.app')
@section('title','Listar todos usuários')
@section('content')
<div class="container">
    <div class="row">
        <div class="offset-4 col-4">
            <h4 class="text-center">Cadastrar usuário</h4>
            <form action="{{ route('user.store') }}" method="post">
                @csrf
                <div class="form-group">
                    <label for="name">Nome</label>
                    <input class="form-control" type="text" id="name" name="name">
                </div>
                <div class="form-group">
                    <label for="email">E-mail</label>
                    <input class="form-control" type="email" id="email" name="email"">
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input class="form-control" type="password" id="password" name="password">
                </div>
                <button class="btn btn-success btn-block" type="submit">Cadastrar</button>
            </form>
        </div>
    </div>
</div>
@endsection
