@extends('layouts.app')
@section('title','Posts do usuário')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-12">
            <h4>Posts do usuário</h4>
        </div>
    </div>
    <hr>
    <div class="row mt-4">
        <div class="col-6">
            @if (isset($user))
                <h5 class="text-primary">Dados do usuário</h5>
                Nome: {{ $user->name }}<br>
                E-mail: {{ $user->email }}              
            @endif  
            
            @if ($address)
                <h5 class="text-primary mt-4">Endereço</h5>
                {{ $address->street }}, {{ $address->number }}, {{ $address->city }} / {{ $address->state }}
            @endif
        </div>
        <div class="col-6">
            @if ($posts and count($posts))
                <h5 class="text-primary">Posts</h5>
                @foreach ($posts as $post)
                    <p>
                        #{{ $post->id }}<br>
                        <strong class="text-secondary">Título: </strong>{{ $post->title }}<br>
                        <strong class="text-secondary">Sub-título: </strong>{{ $post->subtitle }}<br>
                        <strong class="text-secondary">Conteúdo: </strong>{{ $post->content }}<br>
                        <strong class="text-secondary">Criado em: </strong>{{ date('d/m/Y H:i',strtotime($post->created_at)) }}
                    </p>
                @endforeach
            @endif
        </div>
    </div>
</div>
@endsection