@extends('layouts.app')
@section('title','Listar usuários')
@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <h4 class="font-weight-bold text-center mb-5">Lista de usuários</h4>
            <table class="table table-striped table-sm">
                <thead class="thead-light">
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>E-mail</th>
                        <th colspan="3"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                    <tr>
                        <td>{{ $user->id }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td class="text-right"><a class="btn btn-info btn-sm" href="{{ route('user.posts',['id'=>$user->id]) }}">Postagens</a></td>
                        <td class="text-right"><a class="btn btn-secondary btn-sm" href="{{ route('user.edit',['user' => $user->id]) }}">Detalhes</a></td>
                        <td class="text-right">
                            <form action="{{ route('user.destroy', ['user' => $user->id]) }}" method="post">
                                @csrf
                                @method('delete')
                                <input type="hidden" name="user" value="{{ $user->id }}">
                                <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
