<?php

use App\Http\Controllers\AddressController;

use App\Http\Controllers\Form\TestController;
use App\Http\Controllers\HomeController as Home;
use App\Http\Controllers\PostController as Post;
use App\Http\Controllers\CategoryController as Category;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Users\UserController as User;

/*

    Episódio 004 concluído. Renomenado rotas e resources criados via artisan

    Comando para criar o controler, métodos (resource) e o model User
    $ php artisan make:controller Form\\TestController --resource --model=User


*/


Route::get('/',[Home::class,'index'])->name('home');

Route::get('usuarios', [User::class,'all'])->name('user.all');
Route::get('usuarios/novo', [User::class,'formAdd'])->name('user.form.add');
Route::post('usuarios/store',[User::class,'store'])->name('user.store');
Route::get('usuario/{user}/editar', [User::class,'edit'])->name('user.edit');
Route::put('usuario/{user}/edit',[User::class,'update'])->name('user.update');
Route::delete('usuario/{user}/destroy',[User::class,'destroy'])->name('user.destroy');
Route::get('/usuario/{id}/posts',[User::class,'posts'])->name('user.posts');

Route::get('posts',[Post::class,'index'])->name('post.index');
Route::get('/post/novo', [Post::class,'create'])->name('post.create');
Route::post('/post/store', [Post::class,'store'])->name('post.store');
Route::get('post/{post}/show',[Post::class,'show'])->name('post.show');
Route::delete('post/{post}/destroy',[Post::class,'destroy'])->name('post.destroy');

Route::get('categorias',[Category::class,'index'])->name('category.index');
Route::get('categoria/{category}/show',[Category::class,'show'])->name('category.show');
Route::delete('categoria/{category}/destroy',[Category::class,'destroy'])->name('category.destroy');

// // Route::get('listagem-usuario', [UserController::class,'listUser'])->name('user.list');

// Route::get('endereco/{address}',[AddressController::class,'show'])->name('address.show');
//

// Route::get('usuarios/{user}', [TestController::class,'listUser'])->name('user.list');
