<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index()
    {
        $posts = Post::all();

        return view('posts.index',['posts' => $posts]);
    }

    public function create()
    {
        return view('posts.create');
    }

    public function store(Request $req)
    {
        $post = new Post();
        $post->author = 1;
        $post->title = $req->title;
        $post->subtitle = $req->subtitle;
        $post->content = $req->content;
        $post->save();

        //  Cadastra diretamente do request (max assigment), exceto o campo _token
        //$post->create($req->except('_token'));

        return redirect()->route('post.index');
    }

    public function show(Post $post)
    {
        return view('posts.show', [
            'author' => $post->author()->first(),
            'post' => $post,
            'categories' => $post->categories()->get()
        ]);
    }

    public function destroy(Post $post)
    {
        $post->delete();

        return redirect()->route('post.index');
    }
}
