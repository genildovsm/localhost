<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function listUser(){

        // $user = new User();
        // $user->name = "Administrator";
        // $user->email = "admin@gmail.com";
        // $user->password = Hash::make("senha-secreta");
        // $user->save();

        $user = User::where('id','=','1')->first();

        return view('listUser', ['user' => $user]);
    }

    public function show($id)
    {
        $user = User::where('id', $id)->first();        
        $address = $user ? $user->address()->first() : false;        
        $posts = $user ? $user->posts()->get() : false;

        if ($user)
            echo "<h3>Usuário</h3>Nome: {$user->name}<br>Email: {$user->email}";        
        
        if ($address)
            echo "<p>Endereço: {$address->street}, {$address->number}, {$address->city} / {$address->state}.</p>";
    
        if ($posts)
            foreach($posts as $post)
                echo "<p>ID artigo: #{$post->id}<br>Título: {$post->title}<br>Sub-título: {$post->subtitle}<br>Conteúdo: {$post->content}</p>";
    }
}
