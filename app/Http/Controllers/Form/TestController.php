<?php

namespace App\Http\Controllers\Form;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class TestController extends Controller
{
    public function listAllUsers()
    {
        $users = User::all();

        return view('listAllusers', ['users' => $users]);
    }

    public function listUser(User $user)
    {
        return view('listUser', ['user' => $user]);
    }

    public function formAddUser()
    {
        return view('addUser');
    }

    public function storeUser(Request $req)
    {
        $user = new User();
        $user->name = $req->name;
        $user->email = $req->email;
        $user->password = Hash::make($req->password);
        $user->save();

        return redirect()->route('user.listAll');
    }

    public function formEditUser(User $user)
    {
        return view('editUser', ['user' => $user]);
    }

    public function edit(User $user, Request $req)
    {
        $user->name = $req->name;

        if ( filter_var($req->email, FILTER_VALIDATE_EMAIL) )
            $user->email = $req->email;

        if ( strlen(trim($req->password)) )
            $user->password = Hash::make($req->password);

        $user->save();

        return redirect()->route('user.listAll');
    }

    public function destroy(User $user)
    {
        $user->delete();

        return redirect()->route('user.listAll');
    }
}
