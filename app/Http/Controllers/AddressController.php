<?php

namespace App\Http\Controllers;

use App\Models\Address;
use Illuminate\Http\Request;

class AddressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Addresses  $addresses
     * @return \Illuminate\Http\Response
     */
    public function show(Address $address)
    {
        $user = $address->user()->first();

        if ($user)
            echo "<h3>Usuário</h3>Nome : {$user->name}<br>Email : {$user->email}"; 

        if ($address)
            echo "<p>Endereço : {$address->street}, {$address->number}, {$address->city} / {$address->state}.</p>";
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Addresses  $addresses
     * @return \Illuminate\Http\Response
     */
    public function edit(Address $addresses)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Addresses  $addresses
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Addresses $addresses)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Addresses  $addresses
     * @return \Illuminate\Http\Response
     */
    public function destroy(Addresses $addresses)
    {
        //
    }
}
