<?php

namespace App\Http\Controllers\Users;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function all()
    {
        $users = User::all();

        return view('users.all', ['users' => $users]);
    }

    public function listUser(User $user)
    {
        return view('listUser', ['user' => $user]);
    }

    public function formAdd()
    {
        return view('users.add');
    }

    public function store(Request $req)
    {
        $user = new User();
        $user->name = $req->name;
        $user->email = $req->email;
        $user->password = Hash::make($req->password);
        $user->save();

        return redirect()->route('user.all');
    }

    public function edit(User $user)
    {
        return view('users.edit', ['user' => $user]);
    }

    public function update(User $user, Request $req)
    {
        $user->name = $req->name;

        if ( filter_var($req->email, FILTER_VALIDATE_EMAIL) )
            $user->email = $req->email;

        if ( strlen(trim($req->password)) )
            $user->password = Hash::make($req->password);

        $user->save();

        return redirect()->route('user.all');
    }

    public function destroy(User $user)
    {
        $user->delete();

        return redirect()->route('user.all');
    }

    public function posts($id)
    {
        $user = User::where('id', $id)->first();        
        $address = $user ? $user->address()->first() : false;        
        $posts = $user ? $user->posts()->get() : false;

        return view('users.posts',[
            'user' => $user,
            'address' => $address,
            'posts' => $posts
            ]);
    }
}
