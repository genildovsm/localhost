<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Parâmetros 
     * (
     *      Model da tabela Address, 
     *      Campo da tabela addresses que contém a chave estrangeira, 
     *      e o campo da tabela users que faz a ligação
     * )
     */
    public function address()
    {
        return $this->hasOne(Address::class,'user','id');
    }

    public function posts()
    {
        return $this->hasMany(Post::class,'author','id');
    }
}
