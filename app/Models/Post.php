<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Post extends Model
{
    use HasFactory;

    protected $table = 'posts';

    protected $fillable = [
                            'title',
                            'subtitle',
                            'content'
                        ];

    public function setTitleAttribute($value)
    {
        $this->attributes['title'] = $value;
        $this->attributes['slug'] = Str::slug($value);
    }

    public function author()
    {
        return $this->belongsTo(User::class,'author','id');
    }

    /**
     * Relacionamento N -> N
     * Parâmetros
     * Tabela a qual vai ser relacionada com POSTS,
     * a tabela pivot,
     * o campo da tabela pivot que pertence a esse model,
     * o campo que pertence a outra tabela de ligação
     *
     * @return void
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class,'posts_categories','post','category');
    }
}
