<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

/**
 * Executar esse Seed:
 * $ php artisan db:seed --class=UserSeeder
 */

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
        // Forma manual de popular as tabelas do banco

        $user = User::create([
                    'name' => 'Fernanda Smith Gouveia',
                    'email' => 'fernandag@clinicamedica.com.br',
                    'password' => Hash::make('123456')
                ]);

                $user->address()->create([
                    'street'    => 'Rua das Quaresmeiras Verdes',
                    'number'    => '654',
                    'city'      => 'Atibaia',
                    'state'     => 'São Paulo'
                ]);

                $user->posts()->create([
                    'title'     => 'Meu primeiro seeder no framework',
                    'subtitle'  => 'Criando seeder simples',
                    'content'   => 'Conteúdo abordado pertence ao conjunto de vídeo da internet'
                ]);
        */








    }
}
