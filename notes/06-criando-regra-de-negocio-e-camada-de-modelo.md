## 06 - CRIANDO REGRA DE NEGÓCIO E CAMADA DE MODELO

Criar a migration **Posts**
```
php artisan make:migration create_posts_table
```
Criar o controller **Post**
```
php artisan make:controller PostController
```
Criar o model **Post**
```
php artisan make:model Post
```
É possível personalizar o valor recebido para um determinado campo, alterar o *Model* **Post** por exemplo, incluindo o seguinte método:
```
...

public function setTitleAttribute($value)
{
    $this->attributes['title'] = $value;
    $this->attributes['slug'] = Str::slug($value);
}
```
Onde `set` e `Attribute` são palavras reservadas e `Title` o nome do campo que contém o valor a ser tratado.

Criar o modelo, `migration`, `controller` e `resource` com um único comando.
```
php artisan make:model Product -mcr
```

