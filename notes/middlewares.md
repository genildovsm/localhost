## Middlewares

Link do tutorial, [aqui](https://youtu.be/YwHRSe9_zpI?t=320).

No mesmo canal verificar os demais topicos inclusive paginação e filtros de pesquisa.


Os middlewares existentes ficam em: `app\Http\Kernel.php`

Criando um `middleware`
```
php artisan make:middleware CheckIsAdminMiddleware
```

Será criado o arquivo em: `app\Http\Middleware\CheckIsAdminMiddleware.php`
